/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Users;

import Business.Abstract.User;
import java.util.Date;

/**
 *
 * @author Garima
 */
public class Customer extends User implements Comparable<Customer>{
    
    private Date dateCustCreated;

    public Customer(String password, String userName){
        super(password, userName,"CUSTOMER");
        dateCustCreated = new Date();
    }
    
    public Date getDateCustCreated() {
        return dateCustCreated;
    }
    
    @Override
    public boolean verify(String password){
        if (password.equals(this.getPassword()))
            return true;
        return false;
    }

    @Override
    public int compareTo(Customer c) {
        return c.getUserName().compareTo(this.getUserName());        
    }
    
    @Override
    public String toString(){
        return getUserName();
    }
}
